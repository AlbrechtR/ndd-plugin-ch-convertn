﻿#include <qobject.h>
#include <qstring.h>
#include <pluginGl.h>
#include <functional>
#include <qsciscintilla.h>
#include <QAction>
#include "ZhConvertclass.h"

#include <QFile>
#define NDD_EXPORTDLL

#if defined(Q_OS_WIN)
#if defined(NDD_EXPORTDLL)
#define NDD_EXPORT __declspec(dllexport)
#else
#define NDD_EXPORT __declspec(dllimport)
#endif
#else
#define NDD_EXPORT __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

	NDD_EXPORT bool NDD_PROC_IDENTIFY(NDD_PROC_DATA* pProcData);
	NDD_EXPORT int NDD_PROC_MAIN(QWidget* pNotepad, const QString& strFileName, std::function<QsciScintilla* ()>getCurEdit, std::function<bool(int, void*)> pluginCallBack, NDD_PROC_DATA* procData);

#ifdef __cplusplus
}
#endif

static NDD_PROC_DATA s_procData;
static QWidget* s_pMainNotepad = nullptr;
std::function<QsciScintilla* ()> s_getCurEdit;
ZhConvertClass* nddZhConvertPlugin = nullptr;

#pragma execution_character_set("utf-8")
bool NDD_PROC_IDENTIFY(NDD_PROC_DATA* pProcData)
{
	if (pProcData == NULL)
	{
		return false;
	}
	pProcData->m_strPlugName = QObject::tr("繁简中文转换插件");
	pProcData->m_strComment = QObject::tr("繁简中文转换插件(可修改校正词汇表).");

	pProcData->m_version = QString("v1.4");
	pProcData->m_auther = QString("Albrecht");

	pProcData->m_menuType = 1;
	return true;
}

//则点击菜单栏按钮时，会自动调用到该插件的入口点函数。
//pNotepad:就是CCNotepad的主界面指针
//strFileName:当前插件DLL的全路径，如果不关心，则可以不使用
//getCurEdit:从NDD主程序传递过来的仿函数，通过该函数获取当前编辑框操作对象QsciScintilla
//pProcData:如果pProcData->m_menuType = 0 ,则该指针为空；如果pProcData->m_menuType = 1，则该指针有值。目前需要关心s_procData.m_rootMenu
//开发者可以在该菜单下面，自行创建二级菜单
int NDD_PROC_MAIN(QWidget* pNotepad, const QString& strFileName, std::function<QsciScintilla* ()>getCurEdit, std::function<bool(int, void*)> pluginCallBack, NDD_PROC_DATA* pProcData)
{

	//务必拷贝一份pProcData，在外面会释放。
	if (pProcData != nullptr)
	{
		s_procData = *pProcData;
	}
	else
	{
		return -1;
	}

	s_pMainNotepad = pNotepad;
	s_getCurEdit = getCurEdit;

	//如果pProcData->m_menuType = 1;是自己要创建二级菜单的场景。则通过s_procData.m_rootMenu 获取该插件的菜单根节点。
	//插件开发者自行在s_procData.m_rootMenu下添加新的二级菜单项目
	//ZhConvertClass* p = new ZhConvertClass(pNotepad, getCurEdit);
	if (!nddZhConvertPlugin)
	{
		nddZhConvertPlugin = new ZhConvertClass(s_pMainNotepad, strFileName, nullptr, s_pMainNotepad);
	}
	nddZhConvertPlugin->setScintilla(s_getCurEdit);
	QAction* pAction1 = new QAction(s_procData.m_strPlugName, s_procData.m_rootMenu);
	s_procData.m_rootMenu->addAction(pAction1);
	//pAction1->setText(QString::fromUtf8(u8"简体转繁体"));
	pAction1->setStatusTip(("Change from simplified Chinese to traditional Chinese"));
	QObject::connect(pAction1, SIGNAL(triggered(bool)), nddZhConvertPlugin, SLOT(on_SimpletoTra()));

	QAction* pAction2 = new QAction(s_procData.m_strPlugName, s_procData.m_rootMenu);
	s_procData.m_rootMenu->addAction(pAction2);
	//pAction2->setText(QString::fromUtf8(u8"繁体转简体"));
	QObject::connect(pAction2, SIGNAL(triggered(bool)), nddZhConvertPlugin, SLOT(on_TraToSimple()));
	pAction2->setStatusTip(("Traditional Chinese is transformed into simplified Chinese"));

	/*QAction* pAction3 = new QAction(s_procData.m_strPlugName, s_procData.m_rootMenu);
	s_procData.m_rootMenu->addAction(pAction3);
	pAction3->setText(QString::fromUtf8(u8"只校正词汇"));
	QObject::connect(pAction3, SIGNAL(triggered(bool)), nddZhConvertPlugin, SLOT(on_ChangeFix()));*/

	

	QString filePath = "./plugin/SipTradFix.txt"; 
	QFile file(filePath);
	if (file.exists())
	{
		pAction1->setText(QString::fromUtf8(u8"简体转繁体 - 已启用校正表"));
		pAction2->setText(QString::fromUtf8(u8"繁体转简体 - 已启用校正表"));
		QAction* pAction4 = new QAction(s_procData.m_strPlugName, s_procData.m_rootMenu);
		s_procData.m_rootMenu->addAction(pAction4);
		pAction4->setText(QString::fromUtf8(u8"修改校正词汇表"));
		QObject::connect(pAction4, SIGNAL(triggered(bool)), nddZhConvertPlugin, SLOT(on_OpenTradFix()));
	}
	else
	{
		pAction1->setText(QString::fromUtf8(u8"简体转繁体 - 未启用校正表"));
		pAction2->setText(QString::fromUtf8(u8"繁体转简体 - 未启用校正表"));
	}
	

	return 0;
}
