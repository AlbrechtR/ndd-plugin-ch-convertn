# 简体中文与繁体中文互转

## 插件开发说明：

开发使用的**IDE**为 <font color = red>Visual Studio 2022</font>，QT版本为 <font color = red>5.15.2</font>，ndd 版本<font color = red>V1.22</font>， 插件基于 GBK/GB2312 字符编码转换，GBK编码简体部分转换成繁体（基于Windows API） 。



### 繁简体中文的互转方式：

1. 简体中文可以用哪些编码实现: GBK编码简体部分, Unicode编码简体部分, GB2312编码;

2. 繁体中文可以用哪些编码实现: GBK编码繁体部分, Unicode编码繁体部分, BIG5编码;

3. 所以繁简中文互换有这些途径(GBK编码简体部分和GB2312编码可以认为是同一个东西):

	```shell
	a. GBK编码简体部分<->GBK编码繁体部分
	b. GBK编码简体部分<->Unicode编码繁体部分
	c. GBK编码简体部分<->BIG5编码
	d. Unicode编码简体部分<->GBK编码繁体部分
	e. Unicode编码简体部分<->Unicode编码繁体部分
	f. Unicode编码简体部分<->BIG5编码
	```
	
4. 其中编码实现时, "GBK编码<->BIG5编码"需要这样子(通过Unicode作为中间人): GBK编码<->Unicode编码<->BIG5编码, 所以有:
	
	```shell
	A. GBK编码简体部分<->GBK编码繁体部分			变成: GBK编码简体部分<--LCMapStringA-->GBK编码繁体部分
	B. GBK编码简体部分<->Unicode编码繁体部分		变成: GBK编码简体部分<--LCMapStringA-->GBK编码繁体部分<->Unicode编码繁体部分
	C. GBK编码简体部分<->BIG5编码				  变成: GBK编码简体部分<--LCMapStringA-->GBK编码繁体部分<->Unicode编码繁体部分<->BIG5编码
	D. Unicode编码简体部分<->GBK编码繁体部分		变成: Unicode编码简体部分<->GBK编码简体部分<--LCMapStringA-->GBK编码繁体部分
	E. Unicode编码简体部分<->Unicode编码繁体部分	变成: Unicode编码简体部分<->GBK编码简体部分<--LCMapStringA-->GBK编码繁体部分<->Unicode编码繁体部分
	F. Unicode编码简体部分<->BIG5编码			  变成: Unicode编码简体部分<->GBK编码简体部分<--LCMapStringA-->GBK编
	```
	
	原文链接：https://blog.csdn.net/bagboy_taobao_com/article/details/42296515



## 插件使用注意事项：

1. 仅支持**UTF8-BOM** /  **UTF-8** 编码文件进行繁简字体转换**





## 插件安装使用：

**插件使用需要包含一下两个文件**

>SipTradFix.txt  /\* 校正转换词汇表 ,需放置在Notepad--/plugin目录下 \*/ 
>
> 
>
>ZhConvert.dll   /\*插件本件，需放置在Notepad--/plugin目录下\*/

