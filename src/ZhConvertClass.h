#pragma once

#include <QObject>
#include <QWidget>

class QsciScintilla;

class ZhConvertClass : public QObject
{
	Q_OBJECT

public:
	explicit ZhConvertClass(QWidget* mainWidget, const QString& pluginPath, QsciScintilla* pEdit,
		QObject* parent = nullptr);
	~ZhConvertClass();

private slots:
	void on_TraToSimple();
	void on_SimpletoTra();
	void on_OpenTradFix();
	void on_ChangeFix();

public:
	void setScintilla(const std::function<QsciScintilla* ()>& cb);

private:
	std::function<QsciScintilla* ()> scintillaCallback_;
	QString  intext_;

};
